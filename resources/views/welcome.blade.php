<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
         

            <div class="content">
                <div class="title m-b-md">
                  Excel File Uploader Project.
                </div>
                <form id="uploadForm" enctype="multipart/form-data">
                    <div class="form-group">
                      <label for="exampleFormControlFile1">Upload Excell file</label>
                      <input type="file" class="form-control-file" id="file" name="file" required>
                      <br>
                      <hr class="alert alert-warning">
                      <div class="row col-md04 offset-4">
                        <button class="btn btn-success" id="button">Upload Data</button>
                      </div>
                    </div>
                  </form>
                <div >
            
                </div>
            </div>
        </div>
        <script>
     $( document ).ready(function() {
         $( "#uploadForm" ).submit(function(e) {
           e.preventDefault();
           console.log("good")     
        var form = $('#uploadForm')[0];
            var data = new FormData(form);
            console.log(data);
            $.ajax({
            type: "POST",
            url: "/api/uploadFile",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {

            if(data.code == "200"){
                alert("file Uploaded successfully")
            alert('Data Uploaded Successfully')
            }else{ 
                
                alert(JSON.stringify(data.error));
             }    
            
        },
            error: function (e) {
            console.log(e)
               alert(JSON.stringify(e))
            }
          });  
                })
             });
        </script>
    </body>
</html>
