<?php

namespace App\Http\Controllers;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Validator;
class excelUploadController extends Controller
{
    //
    public function uploadFile(request $request){
        $validator = Validator::make($request->all(), [
            'file'   => 'required',
            ]);

      if ($validator->fails()) {

            $response['code'] = 400;
            $response['error'] = $validator->messages();
            return response()->json($response ,200);
      }
      $file = $request->file('file');
      Excel::import(new UsersImport, $file);
            $response['code'] = 200;
            $response['message'] = "File Uploaded";
            return response()->json($response ,200);
    }
}
